import Document, {Head, Html, Main, NextScript} from 'next/document'
import React from "react";
import {FB_APP_ID} from "../config/FB_APP_ID";

class MyDocument extends Document<{ lang }> {

	render() {
		return (
			<Html>
				<Head>
					<meta property="fb:app_id" content={FB_APP_ID}/>
					<meta name="facebook-domain-verification" content="hecszicpja16o7aj1ajofq84bve091"/>
				</Head>
				<body>
				<Main/>
				<NextScript/>
				</body>
			</Html>
		)
	}
}

export default MyDocument;