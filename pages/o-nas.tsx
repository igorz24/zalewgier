import Layout from '../components/layout/layout/layout'
import React, {useEffect} from "react";
import Parallax from "../components/content/parallax/Parallax";
import FlexContentContainer from "../components/content/containers/FlexContentContainer";
import TwoLineHeader from "../components/content/headers/TwoLineHeader";
import VerticalContentContainer from "../components/content/containers/VerticalContentContainer";
import CirclePhoto from "../components/content/containers/CirclePhoto";
import LazyLoad from 'react-lazyload';
import {checkWebpSupport} from "../logic/utils/checkWebpSupport";
import {useI18n} from "next-localization";
import {useRouter} from "next/router";
import {NextSeo, NextSeoProps} from "next-seo";
import {currentLocalizedUrl} from "../logic/utils/buildURL";
import {getMeta} from "../logic/utils/meta-tags";
import Image from 'next/image'
import fireupLogo from "../public/images/logos/fireup.svg"

const localeKey = "about";

const About = () => {
	const webpsupp = checkWebpSupport();
	const router = useRouter()
	const i18n = useI18n();

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../locales/en.json'));
				i18n.locale('en');
			}
		}

		changeLocale();
	}, [router.locale]);

	const {title, desc} = getMeta(router.locale, localeKey)

  const cryptidSrc = "/images/covers/cryptid.jpg"

	const CustomSeo: NextSeoProps = {
		title,
		description: desc,
		openGraph: {
			title,
			description: desc,
			type: 'website',
			url: currentLocalizedUrl(router),
			images: [
				{
					url: cryptidSrc,
					width: 2114,
					height: 1409,
					alt: 'Cryptid',
				},
			],
			locale: router.locale
		}
	}

	return (
		<Layout>
			<NextSeo
				title={CustomSeo.title}
				description={CustomSeo.description}
				openGraph={CustomSeo.openGraph}
			/>
			<Parallax image={webpsupp ? "/images/covers/cryptid.webp" : cryptidSrc} bottomGradient={true} amount={-20}>
				<TwoLineHeader firstLine={i18n.t('about.head-first-line')} secondLine={i18n.t('about.head-second-line')}/>
			</Parallax>

			<VerticalContentContainer webpSupport={webpsupp}>
				<h1>{i18n.t('about.s1.h1')}</h1>
				<p>{i18n.t('about.s1.p1')}</p>
				<p>{i18n.t('about.s1.p2')}</p>
				<p>{i18n.t('about.s1.p3')}</p>
			</VerticalContentContainer>
			<VerticalContentContainer webpSupport={webpsupp} displayLogo={false}>
				<h1>{i18n.t('about.s2.h1')}</h1>
			</VerticalContentContainer>

			<FlexContentContainer>
				<div>
					<h1>Igor Zuber</h1>
					<CirclePhoto src={"/images/faces/igor.jpg"} alt={"Igor Zuber"}/>
				</div>
				<div>
					<h1>Denis Mandrysz</h1>
					<CirclePhoto src={"/images/faces/denis.jpg"} alt={"Denis Mandrysz"}/>
				</div>
				<div>
					<h1>Dawid Horny</h1>
					<CirclePhoto src={"/images/faces/dawid.jpg"} alt={"Dawid Horny"}/>
				</div>
				<div>
					<h1>Michał Pawela</h1>
					<CirclePhoto src={"/images/faces/michal.jpg"} alt={"Michał Pawela"}/>
				</div>
        <div>
          <h1>Dominik Sass</h1>
          <CirclePhoto src={"/images/faces/dominik.jpg"} alt={"Dominik Sass"}/>
        </div>
			</FlexContentContainer>
			<VerticalContentContainer webpSupport={webpsupp} displayLogo={false}>
				<h1>{i18n.t('about.s3.h1')}</h1>
				<a href="https://fireup.pro/">
					<Image src={fireupLogo} width={600} height={200}/>
				</a>
				<p style={{textAlign: "justify", fontSize: "1.2em"}}>
					{i18n.t('about.s3.p1')}
				</p>
			</VerticalContentContainer>

			<LazyLoad offset={200} height={700} once>
				<Parallax image={webpsupp ? "/images/covers/jorvik.webp" : "/images/covers/jorvik.jpg"} topGradient={true} amount={-20}/>
			</LazyLoad>
		</Layout>
	)
}

export default About;
