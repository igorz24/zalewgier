import Layout from '../components/layout/layout/layout'
import {NextSeo, NextSeoProps} from "next-seo";
import React, {useEffect} from "react";
import Parallax from "../components/content/parallax/Parallax";
import TwoLineHeader from "../components/content/headers/TwoLineHeader";
import FlexContentContainer from "../components/content/containers/FlexContentContainer";
import LazyLoad from 'react-lazyload';

import {useI18n} from "next-localization";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../logic/utils/buildURL";
import {getMeta} from "../logic/utils/meta-tags";
import BorderedPhoto from "../components/content/containers/BorderedPhoto";
import {checkWebpSupport} from "../logic/utils/checkWebpSupport";

const localeKey = "offer";

const Offer = () => {
  const webpsupp = checkWebpSupport();
  const router = useRouter()
  const i18n = useI18n();

  useEffect(() => {
    async function changeLocale() {
      if (router.locale === 'pl') {
        i18n.set('pl', await import('../locales/pl.json'));
        i18n.locale('pl');
      } else if (router.locale === 'en') {
        i18n.set('en', await import('../locales/en.json'));
        i18n.locale('en');
      }
    }

    changeLocale();
  }, [router.locale]);

  const {title, desc} = getMeta(router.locale, localeKey)
  const drako = "/images/covers/drako.jpg"

  const CustomSeo: NextSeoProps = {
    title,
    description: desc,
    openGraph: {
      title,
      description: desc,
      type: 'website',
      url: currentLocalizedUrl(router),
      images: [
        {
          url: drako,
          width: 2063,
          height: 1375,
          alt: 'Drako',
        },
      ],
      locale: router.locale
    }
  }

  return (
      <Layout>
        <NextSeo {...CustomSeo}/>
        <Parallax image={webpsupp ? "/images/covers/drako.webp" : drako} bottomGradient={true} amount={-20}>
          <TwoLineHeader firstLine={i18n.t('offer.head-first-line')} secondLine={i18n.t('offer.head-second-line')}/>
        </Parallax>

        <FlexContentContainer>
          <div>
            <h1>{i18n.t('offer.rental')}</h1>
            <BorderedPhoto src={webpsupp ? "/images/offer/collection.webp" : "/images/offer/collection.jpg"} alt={i18n.t('offer.rental')}/>
          </div>
          <div>
            <h1>{i18n.t('offer.company-meetings')}</h1>
            <BorderedPhoto src={webpsupp ? "/images/offer/company.webp" : "/images/offer/company.jpg"} alt={i18n.t('offer.company-meetings')}/>
          </div>
          <div>
            <h1>{i18n.t('offer.rules')}</h1>
            <BorderedPhoto src={webpsupp ? "/images/offer/explanation.webp" : "/images/offer/explanation.jpg"} alt={i18n.t('offer.rules')}/>
          </div>
          <div>
            <h1>{i18n.t('offer.events')}</h1>
            <BorderedPhoto src={webpsupp ? "/images/offer/events.webp" : "/images/offer/events.jpg"} alt={i18n.t('offer.events')}/>
          </div>
          <div>
            <h1>{i18n.t('offer.tournaments')}</h1>
            <BorderedPhoto src={webpsupp ? "/images/offer/tournaments.webp" : "/images/offer/tournaments.jpg"} alt={i18n.t('offer.tournaments')}/>
          </div>
          <div>
            <h1>{i18n.t('offer.presentations')}</h1>
            <BorderedPhoto src={webpsupp ? "/images/offer/presentations.webp" : "/images/offer/presentations.jpg"} alt={i18n.t('offer.presentations')}/>
          </div>
          <div>
            <h1>{i18n.t('offer.prototyping')}</h1>
            <BorderedPhoto src={webpsupp ? "/images/offer/prototypes.webp" : "/images/offer/prototypes.jpg"} alt={i18n.t('offer.prototyping')}/>
          </div>
        </FlexContentContainer>

        <LazyLoad offset={200} height={700} once>
          <Parallax image={webpsupp ? "/images/covers/hanabi.webp" : "/images/covers/hanabi.jpg"} topGradient={true} amount={-30}/>
        </LazyLoad>
      </Layout>
  )
}

export default Offer;