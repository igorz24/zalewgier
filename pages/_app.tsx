import "../styles/global.css";
import Navbar from "../components/layout/navbar/navbar";
import StickyFooter from "../components/layout/footer/sticky-footer";
import SideDrawer from "../components/side-drawer/side-drawer";
import Backdrop from "../components/side-drawer/backdrop";
import React, {useEffect, useState} from 'react';
import {ParallaxProvider} from "react-scroll-parallax";
import TagManager from 'react-gtm-module'
import {I18nProvider} from "next-localization";
import {withRouter} from "next/router";

const tagManagerArgs = {
	gtmId: 'GTM-N5P6G77'
}

function App({Component, pageProps, router}) {

	const [drawerOpen, setDrawerOpen] = useState(false)

	useEffect(() => {
		TagManager.initialize(tagManagerArgs)
	}, [])

	const drawerToogleHandle = () => {
		setDrawerOpen(prevState => !prevState)
	}

	const backdropClickHandle = () => {
		setDrawerOpen(false)
	}

	let backdrop;
	if (drawerOpen) {
		backdrop = <Backdrop click={backdropClickHandle}/>;
	}

	return (
		router.route.startsWith('/games/') ?
			// certain games pages, no menu and footer
			<main style={{
				overflow: 'hidden',
				width: '100%',
				height: '100%',
				position: 'absolute',
				top: 0,
				right: 0,
				bottom: 0,
				left: 0
			}}>

				<Component {...pageProps} />
			</main> :
			// usual content - menu and footer
      <I18nProvider lngDict={pageProps.lngDict} locale={router.locale}>
        <ParallaxProvider>
          {/*<DefaultSeo { ...DEFAULT_SEO } />*/}
          <div>
            <Navbar drawerClickHandler={drawerToogleHandle}/>
            <SideDrawer active={drawerOpen} clickHandle={backdropClickHandle}/>
            {backdrop}
            <main>
              <Component {...pageProps} />
            </main>
            <StickyFooter/>
          </div>
        </ParallaxProvider>
      </I18nProvider>
	);
}

export default withRouter(App)
