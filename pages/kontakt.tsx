import Layout from '../components/layout/layout/layout'
import {NextSeo, NextSeoProps} from "next-seo";
import React, {useEffect} from "react";
import Parallax from "../components/content/parallax/Parallax";
import OneLineHeader from "../components/content/headers/OneLineHeader";
import FacebookButton from "../components/content/buttons/facebook-button";
import TwoLineHeader from "../components/content/headers/TwoLineHeader";
import FlexContentContainer from "../components/content/containers/FlexContentContainer";
import {checkWebpSupport} from "../logic/utils/checkWebpSupport";
import LazyLoad from 'react-lazyload';
import PhoneButton from "../components/content/buttons/PhoneButton";
import {useI18n} from "next-localization";
import {currentLocalizedUrl} from "../logic/utils/buildURL";
import {useRouter} from "next/router";
import {getMeta} from "../logic/utils/meta-tags";
import Obfuscate from 'react-obfuscate';

const localeKey = "contact";

const mailHeaders = {
	subject: 'Cześć!',
}

const Contact = () => {
  const webpsupp = checkWebpSupport();
  const router = useRouter()
	const i18n = useI18n();

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../locales/en.json'));
				i18n.locale('en');
			}
		}
		changeLocale();
	}, [router.locale]);

	const {title, desc} = getMeta(router.locale, localeKey)

  const kotkiJpg = '/images/covers/kotki.jpg'

	const CustomSeo: NextSeoProps = {
		title,
		description: desc,
		openGraph: {
			title,
			description: desc,
			type: 'website',
			url: currentLocalizedUrl(router),
			images: [
				{
					url: kotkiJpg,
					width: 1853,
					height: 1235,
					alt: 'Kociaki Łobuziaki',
				},
			],
			locale: router.locale
		},

	}

	return (
		<>
			<NextSeo
				title={CustomSeo.title}
				description={CustomSeo.description}
				openGraph={CustomSeo.openGraph}
			/>
			<Layout>
				<Parallax image={webpsupp ? '/images/covers/kotki.webp' : kotkiJpg} bottomGradient={true} amount={-30}>
					<OneLineHeader textLine={i18n.t('contact.head-line')}/>
				</Parallax>

				<FlexContentContainer>
					<div>
						<h1>Facebook</h1>
						<FacebookButton/>
					</div>

					<div>
						<h1>{i18n.t('contact.phone')}</h1>
						<PhoneButton number={"515-603-307"}/>
					</div>

					<div>
						<h1>{i18n.t('contact.email')}</h1>
						<h2>{i18n.t('contact.management')}</h2>
						<p>Igor Zuber - <Obfuscate email="i.zuber@zalewgier.pl" headers={mailHeaders}/></p>
						<p>Dawid Horny - <Obfuscate email="d.horny@zalewgier.pl" headers={mailHeaders}/></p>
						<p>Denis Mandrysz - <Obfuscate email="d.mandrysz@zalewgier.pl" headers={mailHeaders}/></p>
						<p>Michał Pawela - <Obfuscate email="m.pawela@zalewgier.pl" headers={mailHeaders}/></p>
						<h2>{i18n.t('contact.general-matters')}</h2>
						<p><Obfuscate email="kontakt@zalewgier.pl" headers={mailHeaders}/></p>
					</div>
				</FlexContentContainer>

				<LazyLoad offset={200} height={700} once>
					<Parallax image={webpsupp ? "/images/covers/ola.webp" : "/images/covers/ola.jpg"} topGradient={true} amount={-20}>
						<TwoLineHeader firstLine={i18n.t('contact.bottom-first-line')}
						               secondLine={i18n.t('contact.bottom-second-line')}/>
					</Parallax>
				</LazyLoad>
			</Layout>
		</>
	)
};

export default Contact;

