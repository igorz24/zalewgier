import {checkWebpSupport} from "../../logic/utils/checkWebpSupport";
import React, {useEffect} from "react";
import {useI18n} from "next-localization";
import {NextSeo, NextSeoProps} from "next-seo";
import Layout from "../../components/layout/layout/layout";
import BlogPostContainer from "../../components/content/containers/BlogPostContainer";
import LazyLoad from 'react-lazyload';
import Parallax from "../../components/content/parallax/Parallax";
import FacebookButton from "../../components/content/buttons/facebook-button";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../../logic/utils/buildURL";
import {getMeta} from "../../logic/utils/meta-tags";
import TwoLineHeader from "../../components/content/headers/TwoLineHeader";
import {gallery} from "../../logic/utils/gallery";
import imageSize from "image-size";
import {prepareLinks} from "../../logic/utils/prepareLinks";
import {ImagesGallery} from "../../components/gallery/ImagesGallery";
import {getGalleryPhotos} from "../../logic/utils/getGalleryPhotos";
import {bbCoverPhoto, bbCoverPhotoWebp, bbPhotoAlts} from "../../photo-lists/blood-bowl";

const localeKey = "blog.blood-bowl-5";

const Post5 = ({photos}) => {
	const webpsupp = checkWebpSupport();
	const router = useRouter()
	const i18n = useI18n();

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../../locales/en.json'));
				i18n.locale('en');
			}
		}

		changeLocale();
	}, [router.locale]);

	const {title, desc} = getMeta(router.locale, localeKey)

	const CustomSeo: NextSeoProps = {
		title,
		description: desc,
		openGraph: {
			title,
			description: desc,
			type: 'article',
			url: currentLocalizedUrl(router),
			article: {
				publishedTime: '2021-01-24T12:00:00Z',
				modifiedTime: '2021-01-24T12:00:00Z',
				authors: [
					'https://www.linkedin.com/in/igor-zuber-870715174/'
				],
				tags: ['Zalew Gier','Blood Bowl','wywiad'],
			},
			images: [
				{
					url: bbCoverPhoto,
					width: 2357,
					height: 1572,
					alt: 'Blood Bowl',
				},
			],
			locale: router.locale
		}
	}

	return (
		<>
      <NextSeo title={CustomSeo.title} description={CustomSeo.description} openGraph={CustomSeo.openGraph} />
			<Layout home>

				<Parallax
					image={webpsupp ? bbCoverPhotoWebp : bbCoverPhoto}
					bottomGradient={true} amount={-20}>
					<TwoLineHeader firstLine={i18n.t(`${localeKey}.title_part_1`)} secondLine={i18n.t(`${localeKey}.title_part_2`)}/>
					<h2>{i18n.t(`${localeKey}.date`) + "  -  " + i18n.t(`${localeKey}.author`)}</h2>
				</Parallax>

				<BlogPostContainer webpSupport={webpsupp}>
					<h2>{prepareLinks(i18n.t(`${localeKey}.intro`))}</h2>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q1`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a1-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a1-d`))}</p>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q2`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a2-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a2-d`))}</p>

					<ImagesGallery images={getGalleryPhotos(photos, 0, 2, webpsupp)}/>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q3`))}</h2>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a3-d`))}</p>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a3-sh`))}</p>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q5`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a5-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a5-d`))}</p>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q6`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a6-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a6-d`))}</p>

					<ImagesGallery images={getGalleryPhotos(photos, 3, 5, webpsupp)}/>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q7`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a7-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a7-d`))}</p>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q8`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a8-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a8-d`))}</p>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q9`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a9-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a9-d`))}</p>

					<ImagesGallery images={getGalleryPhotos(photos, 6, 8, webpsupp)}/>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q10`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a10-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a10-d`))}</p>

					<h2>{prepareLinks(i18n.t(`${localeKey}.q11`))}</h2>
					<p>{'\t'}<text style={{color:"yellow"}}>Shawass: </text>{prepareLinks(i18n.t(`${localeKey}.a11-sh`))}</p>
					<p>{'\t'}<text style={{color:"#5cec6b"}}>Domingo: </text>{prepareLinks(i18n.t(`${localeKey}.a11-d`))}</p>

					<h2>{prepareLinks(i18n.t(`${localeKey}.outro`))}</h2>
					<h3>{prepareLinks(i18n.t(`${localeKey}.interview-caption`))}</h3>

				</BlogPostContainer>
				<LazyLoad offset={200} height={700} once>
					<Parallax
						image={webpsupp ? bbCoverPhotoWebp : bbCoverPhoto}
						topGradient={true} amount={-20}>
						<div style={{width: "100%"}}>
							<FacebookButton styles={{marginTop: '100px'}}/>
						</div>
					</Parallax>
				</LazyLoad>

			</Layout>
		</>
	);
}

export async function getStaticProps() {
	const count = 9;
	const directory = "5-bloodbowl"

	const photos = gallery(count, directory, bbPhotoAlts)
	for (let i = 1; i <= count; i++) {
		const size = imageSize(`public/images/${directory}/${i}s.jpg`)
		photos[i - 1].width = size.width
		photos[i - 1].height = size.height
	}

	return {
		props: {
			photos
		}
	};
}

export default Post5;