import {checkWebpSupport} from "../../logic/utils/checkWebpSupport";
import React, {useEffect} from "react";
import {useI18n} from "next-localization";
import {NextSeo, NextSeoProps} from "next-seo";
import Layout from "../../components/layout/layout/layout";
import BlogPostContainer from "../../components/content/containers/BlogPostContainer";
import LazyLoad from 'react-lazyload';
import Parallax from "../../components/content/parallax/Parallax";
import FacebookButton from "../../components/content/buttons/facebook-button";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../../logic/utils/buildURL";
import {getMeta} from "../../logic/utils/meta-tags";
import TwoLineHeader from "../../components/content/headers/TwoLineHeader";
import {gallery} from "../../logic/utils/gallery";
import imageSize from "image-size";
import {prepareLinks} from "../../logic/utils/prepareLinks";
import {olekCoverPhoto, olekCoverPhotoWebp, olekPhotoAlts} from "../../photo-lists/jagodzinski-interview";
import {ImagesGallery} from "../../components/gallery/ImagesGallery";
import {getGalleryPhotos} from "../../logic/utils/getGalleryPhotos";

const localeKey = "blog.jagodzinski-aleksander-3";

const Post3 = ({photos}) => {
	const webpsupp = checkWebpSupport();
	const router = useRouter()
	const i18n = useI18n();

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../../locales/en.json'));
				i18n.locale('en');
			}
		}

		changeLocale();
	}, [router.locale]);

	const {title, desc} = getMeta(router.locale, localeKey)

	const CustomSeo: NextSeoProps = {
		title,
		description: desc,
		openGraph: {
			title,
			description: desc,
			type: 'article',
			url: currentLocalizedUrl(router),
			article: {
				publishedTime: '2020-12-06T12:00:00Z',
				modifiedTime: '2020-12-06T12:00:00Z',
				authors: [
					'https://www.linkedin.com/in/igor-zuber-870715174/',
					'https://www.facebook.com/al.jag.35'
				],
				tags: ['KURNUGI','KUR-NU-GI','Aleksander Jagodziński', 'Interview', 'Article'],
			},
			images: [
				{
					url: olekCoverPhoto,
					width: 851,
					height: 315,
					alt: 'Aleksander Jagodziński',
				},
			],
			locale: router.locale
		}
	}

	return (
		<>
      <NextSeo title={CustomSeo.title} description={CustomSeo.description} openGraph={CustomSeo.openGraph} />
			<Layout home>

				<Parallax
					image={webpsupp ? olekCoverPhotoWebp : olekCoverPhoto}
					bottomGradient={true} amount={-20}>
					<TwoLineHeader firstLine={i18n.t(`${localeKey}.title_part_1`)}
					               secondLine={i18n.t(`${localeKey}.title_part_2`)}/>
					<h2>{i18n.t(`${localeKey}.date`) + "  -  " + i18n.t(`${localeKey}.author`)}</h2>
				</Parallax>

				<BlogPostContainer webpSupport={webpsupp}>
					<h1 style={{display: "none"}}>{i18n.t(`${localeKey}.title`)}</h1>
					<h2>{prepareLinks(i18n.t(`${localeKey}.introduction`))}</h2>
					<ImagesGallery images={getGalleryPhotos(photos, 0, 2, webpsupp)}/>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q1`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a1`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q2`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a2`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q3`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a3`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q4`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a4`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q5`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a5`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q6`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a6`))}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 3, 5, webpsupp)}/>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q7`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a7`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q8`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a8`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q9`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a9`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q10`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a10`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q11`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a11`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.a12`))}</h2>
					<ImagesGallery images={getGalleryPhotos(photos, 6, 8, webpsupp)}/>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q13`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a13`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q14`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a14-1`))}</p>
					<p>{prepareLinks(i18n.t(`${localeKey}.a14-2`))}</p>
					<p>{prepareLinks(i18n.t(`${localeKey}.a14-3`))}</p>
					<p>{prepareLinks(i18n.t(`${localeKey}.a14-4`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.a14-5`))}</h2>
					<ImagesGallery images={getGalleryPhotos(photos, 9, 11, webpsupp)}/>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q15`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a15`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q16`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a16`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q17`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a17`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q18`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a18`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q19`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a19`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q20`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a20`))}</p>
					<h2>{prepareLinks(i18n.t(`${localeKey}.q21`))}</h2>
					<p>{prepareLinks(i18n.t(`${localeKey}.a21`))}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 12, 14, webpsupp)}/>
					<h3>{prepareLinks(i18n.t(`${localeKey}.interview-caption`))}</h3>

				</BlogPostContainer>
				<LazyLoad offset={200} height={700} once>
					<Parallax
						image={webpsupp ? olekCoverPhotoWebp : olekCoverPhoto}
						topGradient={true} amount={-20}>
						<div style={{width: "100%"}}>
							<FacebookButton styles={{marginTop: '100px'}}/>
						</div>
					</Parallax>
				</LazyLoad>

			</Layout>
		</>
	);
}

export async function getStaticProps() {
	const count = 15;
	const directory = "3-jagodzinski"

	const photos = gallery(count, directory, olekPhotoAlts)
	for (let i = 1; i <= count; i++) {
		const size = imageSize(`public/images/${directory}/${i}s.jpg`)
		photos[i - 1].width = size.width
		photos[i - 1].height = size.height
	}

	return {
		props: {
			photos
		}
	};
}

export default Post3;