import {checkWebpSupport} from "../../logic/utils/checkWebpSupport";
import React, {useEffect} from "react";
import {useI18n} from "next-localization";
import {NextSeo, NextSeoProps} from "next-seo";
import Layout from "../../components/layout/layout/layout";
import BlogPostContainer from "../../components/content/containers/BlogPostContainer";
import {ImagesGallery} from "../../components/gallery/ImagesGallery";
import LazyLoad from 'react-lazyload';
import Parallax from "../../components/content/parallax/Parallax";
import FacebookButton from "../../components/content/buttons/facebook-button";
import {paperSheetsCoverPhoto, paperSheetsCoverPhotoWebp, paperSheetsPhotoAlts} from "../../photo-lists/paper-sheets";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../../logic/utils/buildURL";
import {getMeta} from "../../logic/utils/meta-tags";
import {getGalleryPhotos} from "../../logic/utils/getGalleryPhotos";
import TwoLineHeader from "../../components/content/headers/TwoLineHeader";
import {gallery} from "../../logic/utils/gallery";
import imageSize from "image-size";


const localeKey = "blog.papierowe-notesy-1";

const Post1 = ({photos}) => {
	const webpsupp = checkWebpSupport();
	const router = useRouter()
	const i18n = useI18n();

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../../locales/en.json'));
				i18n.locale('en');
			}
		}

		changeLocale();
	}, [router.locale]);

	const {title, desc} = getMeta(router.locale, localeKey)

	const CustomSeo: NextSeoProps = {
		title,
		description: desc,
		openGraph: {
			title,
			description: desc,
			type: 'article',
			url: currentLocalizedUrl(router),
			article: {
				publishedTime: '2019-10-09T12:00:00Z',
				modifiedTime: '2020-10-30T12:00:00Z',
				authors: [
					'https://www.linkedin.com/in/igor-zuber-870715174/'
				],
				tags: ['Paper Sheets', 'Article'],
			},
			images: [
				{
					url: paperSheetsCoverPhoto,
					width: 1964,
					height: 1309,
					alt: 'Welcome too... with laminated sheets',
				},
			],
			locale: router.locale
		}
	}

	return (
		<>
      <NextSeo title={CustomSeo.title} description={CustomSeo.description} openGraph={CustomSeo.openGraph} />
			<Layout home>

				<Parallax
					image={webpsupp ? paperSheetsCoverPhotoWebp : paperSheetsCoverPhoto}
					bottomGradient={true} amount={-20}>
					<TwoLineHeader firstLine={i18n.t('blog.papierowe-notesy-1.title_part_1')}
					               secondLine={i18n.t('blog.papierowe-notesy-1.title_part_2')}/>
					<h2>{i18n.t('blog.papierowe-notesy-1.date') + "  -  " + i18n.t('blog.papierowe-notesy-1.author')}</h2>
				</Parallax>

				<BlogPostContainer webpSupport={webpsupp}>
					<p>{i18n.t('blog.papierowe-notesy-1.p1')}</p>
					<p>{i18n.t('blog.papierowe-notesy-1.p2')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 0, 1, webpsupp)}/>
					<p>{i18n.t('blog.papierowe-notesy-1.p3')}</p>
					<p>{i18n.t('blog.papierowe-notesy-1.p4')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 2, 5, webpsupp)}/>
					<p>{i18n.t('blog.papierowe-notesy-1.p5')}</p>
					<p>{i18n.t('blog.papierowe-notesy-1.p6')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 6, 8, webpsupp)}/>
					<p>{i18n.t('blog.papierowe-notesy-1.p7')}</p>
					<p>{i18n.t('blog.papierowe-notesy-1.p8')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 9, 16, webpsupp)}/>
					<p>{i18n.t('blog.papierowe-notesy-1.p9')}</p>
					<p>{i18n.t('blog.papierowe-notesy-1.p10')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 17, 18, webpsupp)}/>
				</BlogPostContainer>
				<LazyLoad offset={200} height={700} once>
					<Parallax
						image={webpsupp ? paperSheetsCoverPhotoWebp : paperSheetsCoverPhoto}
						topGradient={true} amount={-30}>
						<div style={{width: "100%"}}>
							<FacebookButton styles={{marginTop: '100px'}}/>
						</div>
					</Parallax>
				</LazyLoad>

			</Layout>
		</>
	);
}

export async function getStaticProps() {
	const count = 19;
	const directory = "1-notesy"

	const photos =  gallery(count, directory, paperSheetsPhotoAlts)
	for (let i = 1; i <= count; i++) {
		const size = imageSize(`public/images/${directory}/${i}s.jpg`)
		photos[i-1].width = size.width
		photos[i-1].height = size.height
	}

	return {
		props: {
			photos
		}
	};
}

export default Post1;