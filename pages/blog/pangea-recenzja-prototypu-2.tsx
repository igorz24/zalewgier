import {checkWebpSupport} from "../../logic/utils/checkWebpSupport";
import React, {useEffect} from "react";
import {useI18n} from "next-localization";
import {NextSeo, NextSeoProps} from "next-seo";
import Layout from "../../components/layout/layout/layout";
import BlogPostContainer from "../../components/content/containers/BlogPostContainer";
import LazyLoad from 'react-lazyload';
import Parallax from "../../components/content/parallax/Parallax";
import FacebookButton from "../../components/content/buttons/facebook-button";
import {ImagesGallery} from "../../components/gallery/ImagesGallery";
import {pangeaCoverPhoto, pangeaCoverPhotoWebp, pangeaPhotoAlts} from "../../photo-lists/pangea-prototype";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../../logic/utils/buildURL";
import {getMeta} from "../../logic/utils/meta-tags";
import {getGalleryPhotos} from "../../logic/utils/getGalleryPhotos";
import TwoLineHeader from "../../components/content/headers/TwoLineHeader";
import imageSize from "image-size";
import {gallery} from "../../logic/utils/gallery";

const localeKey = "blog.pangea-recenzja-prototypu-2"

const Post2 = ({photos}) => {
	const webpsupp = checkWebpSupport();
	const router = useRouter()
	const i18n = useI18n();

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../../locales/en.json'));
				i18n.locale('en');
			}
		}

		changeLocale();
	}, [router.locale]);

	const {title, desc} = getMeta(router.locale, localeKey)

	const CustomSeo: NextSeoProps = {
		title,
		description: desc,
		openGraph: {
			title,
			description: desc,
			type: 'article',
			url: currentLocalizedUrl(router),
			article: {
				publishedTime: '2019-04-12:00:00Z',
				modifiedTime: '2020-10-30T12:00:00Z',
				authors: [
					'https://www.linkedin.com/in/igor-zuber-870715174/'
				],
				tags: ['Pangea', 'Review'],
			},
			images: [
				{
					url: pangeaCoverPhoto,
					width: 1943,
					height: 1296,
					alt: 'Pangea',
				},
			],
			locale: router.locale
		}
	}

	return (
		<>
      <NextSeo title={CustomSeo.title} description={CustomSeo.description} openGraph={CustomSeo.openGraph} />
			<Layout home>

				<Parallax
					image={webpsupp ? pangeaCoverPhotoWebp : pangeaCoverPhoto}
					bottomGradient={true} amount={-20}>
					<TwoLineHeader firstLine={i18n.t('blog.pangea-recenzja-prototypu-2.title_part_1')}
					               secondLine={i18n.t('blog.pangea-recenzja-prototypu-2.title_part_2')}/>
					<h2>{i18n.t('blog.pangea-recenzja-prototypu-2.date') + "  -  " + i18n.t('blog.pangea-recenzja-prototypu-2.author')}</h2>
				</Parallax>

				<BlogPostContainer webpSupport={webpsupp}>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p1')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p2')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 0, 3, webpsupp)}/>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p3')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p4')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 4, 6, webpsupp)}/>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p5')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 7, 10, webpsupp)}/>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p6')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 11, 15, webpsupp)}/>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p7')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p8')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p9')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p10')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p11')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 16, 23, webpsupp)}/>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p12')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p13')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p14')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p15')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p16')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 24, 28, webpsupp)}/>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p17')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p18')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p19')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 29, 33, webpsupp)}/>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p20')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p21')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p22')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p23')}</p>
					<ImagesGallery images={getGalleryPhotos(photos, 34, 36, webpsupp)}/>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p24')}</p>
					<p>{i18n.t('blog.pangea-recenzja-prototypu-2.p25')}</p>
				</BlogPostContainer>
				<LazyLoad offset={200} height={700} once>
					<Parallax
						image={webpsupp ? pangeaCoverPhotoWebp : pangeaCoverPhoto}
						topGradient={true} amount={-20}>
						<div style={{width: "100%"}}>
							<FacebookButton styles={{marginTop: '100px'}}/>
						</div>
					</Parallax>
				</LazyLoad>

			</Layout>
		</>
	);
}

export async function getStaticProps() {
	const count = 37;
	const directory = "2-pangea"

	const photos =  gallery(count, directory, pangeaPhotoAlts)
	for (let i = 1; i <= count; i++) {
		const size = imageSize(`public/images/${directory}/${i}s.jpg`)
		photos[i-1].width = size.width
		photos[i-1].height = size.height
	}

	return {
		props: {
			photos
		}
	};
}

export default Post2;