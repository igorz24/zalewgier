import {checkWebpSupport} from "../../logic/utils/checkWebpSupport";
import React, {useEffect} from "react";
import {useI18n} from "next-localization";
import {NextSeo, NextSeoProps} from "next-seo";
import Layout from "../../components/layout/layout/layout";
import BlogPostContainer from "../../components/content/containers/BlogPostContainer";
import LazyLoad from 'react-lazyload';
import Parallax from "../../components/content/parallax/Parallax";
import FacebookButton from "../../components/content/buttons/facebook-button";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../../logic/utils/buildURL";
import {getMeta} from "../../logic/utils/meta-tags";
import TwoLineHeader from "../../components/content/headers/TwoLineHeader";
import {gallery} from "../../logic/utils/gallery";
import imageSize from "image-size";
import {prepareLinks} from "../../logic/utils/prepareLinks";
import {ImagesGallery} from "../../components/gallery/ImagesGallery";
import {getGalleryPhotos} from "../../logic/utils/getGalleryPhotos";
import {
    podsumowanie2022cover,
    podsumowanie2022coverWebp,
    podsumowanie2022PhotoAlts
} from "../../photo-lists/podsumowanie-2022";

const localeKey = "blog.podsumowanie-2022-7";

const Post7 = ({photos}) => {
    const webpsupp = checkWebpSupport();
    const router = useRouter()
    const i18n = useI18n();

    useEffect(() => {
        async function changeLocale() {
            if (router.locale === 'pl') {
                i18n.set('pl', await import('../../locales/pl.json'));
                i18n.locale('pl');
            } else if (router.locale === 'en') {
                i18n.set('en', await import('../../locales/en.json'));
                i18n.locale('en');
            }
        }

        changeLocale();
    }, [router.locale]);

    const {title, desc} = getMeta(router.locale, localeKey)

    const CustomSeo: NextSeoProps = {
        title,
        description: desc,
        openGraph: {
            title,
            description: desc,
            type: 'article',
            url: currentLocalizedUrl(router),
            article: {
                publishedTime: '2023-01-06T12:00:00Z',
                modifiedTime: '2023-01-06T12:00:00Z',
                authors: [
                    'https://www.facebook.com/dominik.sass.7',
                    'https://www.linkedin.com/in/igor-zuber-870715174/'
                ],
                tags: ['Zalew Gier','Podsumowanie roku','rok 2022', 'Gry planszowe Rybnik'],
            },
            images: [
                {
                    url: podsumowanie2022cover,
                    width: 2402,
                    height: 1602,
                    alt: 'Root meeples',
                },
            ],
            locale: router.locale
        }
    }

    return (
        <>
            <NextSeo title={CustomSeo.title} description={CustomSeo.description} openGraph={CustomSeo.openGraph} />
            <Layout home>

                <Parallax
                    image={webpsupp ? podsumowanie2022coverWebp : podsumowanie2022cover}
                    bottomGradient={true} amount={-20}>
                    <TwoLineHeader firstLine={i18n.t(`${localeKey}.title_part_1`)} secondLine={i18n.t(`${localeKey}.title_part_2`)}/>
                    <h2>{i18n.t(`${localeKey}.date`) + "  -  " + i18n.t(`${localeKey}.author`)}</h2>
                </Parallax>

                <BlogPostContainer webpSupport={webpsupp}>
                    <h1>{prepareLinks(i18n.t(`${localeKey}.h1`))}</h1>
                    <h2 style={{textAlign:"center"}}>{prepareLinks(i18n.t(`${localeKey}.intro`))}</h2>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p1-1`))}</p>
                    <ImagesGallery images={getGalleryPhotos(photos, 0, 5, webpsupp)}/>

                    <h1 >{prepareLinks(i18n.t(`${localeKey}.h2`))}</h1>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p2-1`))}</p>
                    <ImagesGallery images={getGalleryPhotos(photos, 6, 12, webpsupp)}/>

                    <h1 >{prepareLinks(i18n.t(`${localeKey}.h3`))}</h1>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p3-1`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p3-2`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p3-3`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p3-4`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p3-5`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p3-6`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p3-7`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p3-8`))}</p>
                    <ImagesGallery images={getGalleryPhotos(photos, 13, 18, webpsupp)}/>

                    <h1 >{prepareLinks(i18n.t(`${localeKey}.h4`))}</h1>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p4-1`))}</p>
                    <ImagesGallery images={getGalleryPhotos(photos, 19, 24, webpsupp)}/>

                    <h1 >{prepareLinks(i18n.t(`${localeKey}.h5`))}</h1>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p5-1`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p5-2`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p5-3`))}</p>
                    <ImagesGallery images={getGalleryPhotos(photos, 25, 25, webpsupp)}/>

                    <h1 >{prepareLinks(i18n.t(`${localeKey}.h6`))}</h1>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p6-1`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p6-2`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p6-3`))}</p>
                    <ImagesGallery images={getGalleryPhotos(photos, 26, 26, webpsupp)}/>

                    <h1 >{prepareLinks(i18n.t(`${localeKey}.h7`))}</h1>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p7-1`))}</p>

                    <h1 >{prepareLinks(i18n.t(`${localeKey}.h8`))}</h1>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p8-1`))}</p>

                    <h1 >{prepareLinks(i18n.t(`${localeKey}.h9`))}</h1>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-1`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-2`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-3`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-4`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-5`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-6`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-7`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-8`))}</p>
                    <p>{prepareLinks(i18n.t(`${localeKey}.p9-9`))}</p>

                </BlogPostContainer>
                <LazyLoad offset={200} height={700} once>
                    <Parallax
                        image={webpsupp ? podsumowanie2022coverWebp : podsumowanie2022cover}
                        topGradient={true} amount={-30}>
                        <div style={{width: "100%"}}>
                            <FacebookButton styles={{marginTop: '100px'}}/>
                        </div>
                    </Parallax>
                </LazyLoad>

            </Layout>
        </>
    );
}

export async function getStaticProps() {
    const count = 27;
    const directory = "7-podsumowanie2022"

    const photos = gallery(count, directory, podsumowanie2022PhotoAlts)
    for (let i = 1; i <= count; i++) {
        const size = imageSize(`public/images/${directory}/${i}s.jpg`)
        photos[i - 1].width = size.width
        photos[i - 1].height = size.height
    }

    return {
        props: {
            photos
        }
    };
}

export default Post7;