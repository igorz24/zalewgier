import {checkWebpSupport} from "../../logic/utils/checkWebpSupport";
import React, {useEffect} from "react";
import {useI18n} from "next-localization";
import {NextSeo, NextSeoProps} from "next-seo";
import Layout from "../../components/layout/layout/layout";
import BlogPostContainer from "../../components/content/containers/BlogPostContainer";
import LazyLoad from 'react-lazyload';
import Parallax from "../../components/content/parallax/Parallax";
import FacebookButton from "../../components/content/buttons/facebook-button";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../../logic/utils/buildURL";
import {getMeta} from "../../logic/utils/meta-tags";
import TwoLineHeader from "../../components/content/headers/TwoLineHeader";
import {gallery} from "../../logic/utils/gallery";
import imageSize from "image-size";
import {prepareLinks} from "../../logic/utils/prepareLinks";
import {ImagesGallery} from "../../components/gallery/ImagesGallery";
import {getGalleryPhotos} from "../../logic/utils/getGalleryPhotos";
import {
  podsumowanie2021cover,
  podsumowanie2021coverWebp,
  podsumowanie2021PhotoAlts
} from "../../photo-lists/podusmowanie-2021";

const localeKey = "blog.podsumowanie-2021-6";

const Post6 = ({photos}) => {
  const webpsupp = checkWebpSupport();
  const router = useRouter()
  const i18n = useI18n();

  useEffect(() => {
    async function changeLocale() {
      if (router.locale === 'pl') {
        i18n.set('pl', await import('../../locales/pl.json'));
        i18n.locale('pl');
      } else if (router.locale === 'en') {
        i18n.set('en', await import('../../locales/en.json'));
        i18n.locale('en');
      }
    }

    changeLocale();
  }, [router.locale]);

  const {title, desc} = getMeta(router.locale, localeKey)

  const CustomSeo: NextSeoProps = {
    title,
    description: desc,
    openGraph: {
      title,
      description: desc,
      type: 'article',
      url: currentLocalizedUrl(router),
      article: {
        publishedTime: '2022-01-29T12:00:00Z',
        modifiedTime: '2022-01-29T12:00:00Z',
        authors: [
          'https://www.linkedin.com/in/igor-zuber-870715174/'
        ],
        tags: ['Zalew Gier','Podsumowanie roku','rok 2021', 'Gry planszowe Rybnik'],
      },
      images: [
        {
          url: podsumowanie2021cover,
          width: 2402,
          height: 1602,
          alt: 'Root meeples',
        },
      ],
      locale: router.locale
    }
  }

  return (
      <>
        <NextSeo title={CustomSeo.title} description={CustomSeo.description} openGraph={CustomSeo.openGraph} />
        <Layout home>

          <Parallax
              image={webpsupp ? podsumowanie2021coverWebp : podsumowanie2021cover}
              bottomGradient={true} amount={-20}>
            <TwoLineHeader firstLine={i18n.t(`${localeKey}.title_part_1`)} secondLine={i18n.t(`${localeKey}.title_part_2`)}/>
            <h2>{i18n.t(`${localeKey}.date`) + "  -  " + i18n.t(`${localeKey}.author`)}</h2>
          </Parallax>

          <BlogPostContainer webpSupport={webpsupp}>
            <h1>{prepareLinks(i18n.t(`${localeKey}.h1`))}</h1>
            <h2 style={{textAlign:"center"}}>{prepareLinks(i18n.t(`${localeKey}.intro`))}</h2>
            <p>{prepareLinks(i18n.t(`${localeKey}.p1-1`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p1-2`))}</p>
            <ImagesGallery images={getGalleryPhotos(photos, 0, 8, webpsupp)}/>

            <h1 >{prepareLinks(i18n.t(`${localeKey}.h2`))}</h1>
            <p>{prepareLinks(i18n.t(`${localeKey}.p2-1`))}</p>
            <ImagesGallery images={getGalleryPhotos(photos, 9, 12, webpsupp)}/>

            <h1 >{prepareLinks(i18n.t(`${localeKey}.h3`))}</h1>
            <p>{prepareLinks(i18n.t(`${localeKey}.p3-1`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p3-2`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p3-3`))}</p>
            <ImagesGallery images={getGalleryPhotos(photos, 13, 18, webpsupp)}/>

            <h1 >{prepareLinks(i18n.t(`${localeKey}.h4`))}</h1>
            <p>{prepareLinks(i18n.t(`${localeKey}.p4-1`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p4-2`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p4-3`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p4-4`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p4-5`))}</p>
            <ImagesGallery images={getGalleryPhotos(photos, 19, 21, webpsupp)}/>

            <h1 >{prepareLinks(i18n.t(`${localeKey}.h5`))}</h1>
            <p>{prepareLinks(i18n.t(`${localeKey}.p5-1`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p5-2`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p5-3`))}</p>
            <ImagesGallery images={getGalleryPhotos(photos, 23, 23, webpsupp)}/>

            <h1 >{prepareLinks(i18n.t(`${localeKey}.h6`))}</h1>
            <p>{prepareLinks(i18n.t(`${localeKey}.p6-1`))}</p>

            <h1 >{prepareLinks(i18n.t(`${localeKey}.h7`))}</h1>
            <p>{prepareLinks(i18n.t(`${localeKey}.p7-1`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p7-2`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p7-3`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p7-4`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p7-5`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p7-6`))}</p>
            <p>{prepareLinks(i18n.t(`${localeKey}.p7-7`))}</p>

          </BlogPostContainer>
          <LazyLoad offset={200} height={700} once>
            <Parallax
                image={webpsupp ? podsumowanie2021coverWebp : podsumowanie2021cover}
                topGradient={true} amount={-30}>
              <div style={{width: "100%"}}>
                <FacebookButton styles={{marginTop: '100px'}}/>
              </div>
            </Parallax>
          </LazyLoad>

        </Layout>
      </>
  );
}

export async function getStaticProps() {
  const count = 24;
  const directory = "6-podsumowanie2021"

  const photos = gallery(count, directory, podsumowanie2021PhotoAlts)
  for (let i = 1; i <= count; i++) {
    const size = imageSize(`public/images/${directory}/${i}s.jpg`)
    photos[i - 1].width = size.width
    photos[i - 1].height = size.height
  }

  return {
    props: {
      photos
    }
  };
}

export default Post6;