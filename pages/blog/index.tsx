import React, {useEffect} from "react";
import {useI18n} from "next-localization";
import {NextSeo, NextSeoProps} from "next-seo";
import Layout from "../../components/layout/layout/layout";
import Parallax from "../../components/content/parallax/Parallax";
import OneLineHeader from "../../components/content/headers/OneLineHeader";
import BlogIndexContainer from "../../components/content/containers/BlogIndexContainer";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../../logic/utils/buildURL";
import {getMeta} from "../../logic/utils/meta-tags";
import LazyLoad from 'react-lazyload';
import {checkWebpSupport} from "../../logic/utils/checkWebpSupport";

const localeKey = "blog"

const Blog = () => {
  const webpsupp = checkWebpSupport();
  const router = useRouter()
	const i18n = useI18n();

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../../locales/en.json'));
				i18n.locale('en');
			}
		}
		changeLocale();
	}, [router.locale]);

	const {title, desc} = getMeta(router.locale, localeKey)

  const CustomSeo: NextSeoProps = {
		title,
		description: desc,
		openGraph: {
			title,
			description: desc,
			type: 'website',
			url: currentLocalizedUrl(router),
			images: [
				{
					url: "/images/covers/welcome.jpg",
					width: 1655,
					height: 1103,
					alt: 'Welcome to...',
				},
			],
			locale: router.locale
		}
	}

	return (
		<>
			<NextSeo {...CustomSeo}/>
			<Layout home>
					<Parallax
						image={webpsupp ? "/images/covers/whitechapel.webp" : "/images/covers/whitechapel.jpg"}
						bottomGradient={true} amount={-20}>
						<OneLineHeader textLine={i18n.t('blog.title')}/>
					</Parallax>
					<BlogIndexContainer>
						<h1>{i18n.t('blog.h1')}</h1>
					</BlogIndexContainer>
				<LazyLoad offset={200} height={700} once>
					<Parallax
						image={webpsupp ? "/images/covers/welcome.webp" : "/images/covers/welcome.jpg"}
						topGradient={true} amount={-20}>
					</Parallax>
				</LazyLoad>
			</Layout>
		</>
	);
}

export default Blog;
