import Layout from "../components/layout/layout/layout";
import Parallax from "../components/content/parallax/Parallax";
import LazyLoad from 'react-lazyload';
import React, {useEffect} from 'react';
import {useI18n} from 'next-localization';

import VerticalContentContainer from "../components/content/containers/VerticalContentContainer";
import TwoLineHeader from "../components/content/headers/TwoLineHeader";
import OneLineHeader from "../components/content/headers/OneLineHeader";
import FacebookButton from "../components/content/buttons/facebook-button";
import {NextSeo, NextSeoProps} from "next-seo";
import ParagraphWithDice from "../components/content/styled-paragraph/ParagraphWithDice";
import {checkWebpSupport} from "../logic/utils/checkWebpSupport";
import {useRouter} from "next/router";
import {currentLocalizedUrl} from "../logic/utils/buildURL";

import {getMeta} from "../logic/utils/meta-tags";


const localeKey = "main-page";

const Home = () => {
	const webpsupp = checkWebpSupport();
	const router = useRouter()
	const i18n = useI18n();

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../locales/en.json'));
				i18n.locale('en');
			}
		}
		changeLocale();
	}, [router.locale]);

	const {title, desc} = getMeta(router.locale, localeKey)
  const zg = "/images/zg.png";

	const CustomSeo: NextSeoProps = {
		title,
		description: desc,
		openGraph: {
			title,
			description: desc,
			type: 'website',
			url: currentLocalizedUrl(router),
			images: [
				{
					url: zg,
					width: 900,
					height: 506,
					alt: 'Zalew Gier',
				},
			],
			locale: router.locale
		}
	}

	return (
		<>
      <NextSeo title={CustomSeo.title} description={CustomSeo.description} openGraph={CustomSeo.openGraph} />
			<Layout home>
				<div>
						<Parallax image={webpsupp ? "/images/covers/tm.webp" : "/images/covers/tm.jpg"} bottomGradient={true} amount={-20}>
							<TwoLineHeader firstLine={i18n.t('main-page.head-first-line')} secondLine={i18n.t('main-page.head-second-line')}/>
						</Parallax>
					<VerticalContentContainer webpSupport={webpsupp}>

						<h1>{i18n.t('main-page.s1.h1')}</h1>
						<p>{i18n.t('main-page.s1.p1')}</p>

						<h1>{i18n.t('main-page.s2.h1')}</h1>
						<h2>{i18n.t('main-page.s2.p1')}</h2>
						<ParagraphWithDice>{i18n.t('main-page.s2.p2')}</ParagraphWithDice>
						<ParagraphWithDice>{i18n.t('main-page.s2.p3')}</ParagraphWithDice>
						<ParagraphWithDice>{i18n.t('main-page.s2.p4')}</ParagraphWithDice>

						<h1>{i18n.t('main-page.s3.h1')}</h1>
						<ParagraphWithDice>{i18n.t('main-page.s3.p1')}</ParagraphWithDice>
						<ParagraphWithDice>{i18n.t('main-page.s3.p2')}</ParagraphWithDice>
						<ParagraphWithDice>{i18n.t('main-page.s3.p3')}</ParagraphWithDice>

					</VerticalContentContainer>
					<LazyLoad offset={200} height={700} once>
						<Parallax
							image={webpsupp ? "/images/covers/keyforge.webp" : "/images/covers/keyforge.jpg"}
							topGradient={true} amount={-30}>
							<div style={{width: "100%"}}>
								<OneLineHeader textLine={i18n.t('main-page.end-line')}/>
								<FacebookButton styles={{marginTop: '100px'}}/>
							</div>
						</Parallax>
					</LazyLoad>
				</div>
			</Layout>
		</>
	);
}

export default Home;
