import React, {useCallback, useState} from "react";
React.useLayoutEffect = React.useEffect; // FIX for useLayoutEffect when SSR
import Gallery from "react-photo-gallery";
import Carousel, {Modal, ModalGateway} from "react-images";
import css from "./images-gallery.module.css"

export interface Photo {
	src: string
	srcSet?: string | string[]
	sizes?: string | string[]
	width: number
	height: number
	alt?: string
	key?: string
}

export interface PhotoWebP extends Photo{
	src_webp?: string
	srcSet_webp?: string | string[]
}

export const ImagesGallery = ({images}: { images: PhotoWebP[] }) => {
	const [currentImage, setCurrentImage] = useState(0);
	const [viewerIsOpen, setViewerIsOpen] = useState(false);

	const openLightbox = useCallback((event, {index}) => {
		setCurrentImage(index);
		setViewerIsOpen(true);
	}, []);

	const closeLightbox = () => {
		setCurrentImage(0);
		setViewerIsOpen(false);
	};

	images.forEach(img => img.alt = img.alt ?? "another blog image")

	return (
		<div className={css.gallery_container}>
			<Gallery photos={images} onClick={openLightbox}/>
			<ModalGateway>
				{viewerIsOpen ? (
					<Modal
						onClose={closeLightbox}
						allowFullscreen={true}
						closeOnBackdropClick={true}
					>
						<Carousel
							currentIndex={currentImage}
							views={images.map(x => ({
								...x,
								source: x.src,
								caption: x.alt
							}))}
							styles={{
								container: base => ({
									...base,
									height: '100vh',
								}),
								view: base => ({
									...base,
									alignItems: 'center',
									display: 'flex ',
									height: 'calc(100vh - 10px)',
									justifyContent: 'center',

									'& > img': {
										maxHeight: 'calc(100vh - 14px)',
									},
								}),
							}}
						/>
					</Modal>
				) : null}
			</ModalGateway>
		</div>
	);
}