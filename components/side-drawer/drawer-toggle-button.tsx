import styles from "./side-menu.module.css";


export default function DrawToggleButton(props: {click?: any}) {
  return (
      <button className={styles.switch} onClick={props.click}>
          <div className={styles.toggle_button_line}/>
          <div className={styles.toggle_button_line}/>
          <div className={styles.toggle_button_line}/>
      </button>
  );
}
