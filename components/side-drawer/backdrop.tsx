
import styles from "./backdrop.module.css";

export default function Backdrop(props: {click: any}) {
    return(
    <div className={styles.backdrop} onClick={props.click}/>
    );
}