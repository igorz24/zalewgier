import styles from "./side-drawer.module.css";
import MenuItems from "../layout/navbar/menu-items";
import React from "react";

export default function SideDrawer(props: {active: boolean, clickHandle: any}) {
  let drawerClasses: string = styles.side_drawer;
  if(props.active)
    drawerClasses = styles.side_drawer + ' ' + styles.open;
  return (
    <nav className={drawerClasses} onClick={props.clickHandle}>
      <MenuItems/>
    </nav>
  );
}
