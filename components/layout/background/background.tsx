import styles from "./background.module.css"
import React from "react";

export default function Background({children, image}: { children: React.ReactNode, image: any }) {
    return (
        <div className={styles.background} style={{backgroundImage: `url(${image})`}}>
            <main>{children}</main>
        </div>
    );
}
