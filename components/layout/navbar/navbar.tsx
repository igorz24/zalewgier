import Link from "next/link";
import css from "./navbar.module.css";
import DrawToggleButton from "../../side-drawer/drawer-toggle-button";
import MenuItems from "./menu-items";
import React from "react";
import useScrollPosition from '@react-hook/window-scroll'
import {logo_color_small_png} from "../../../logic/utils/logos";
import Image from 'next/image'

const navId: string = "nav_area";
const navLogoId: string = "nav_logo";

interface NavbarProps {
	drawerClickHandler?: Function
}

export default function Navbar(props: NavbarProps) {
	const top: boolean = useScrollPosition(30) < 100;

	return (
		<header id={navId} className={top ? css.nav_area : css.nav_area_sticky}>
			<div className={css.toolbar}>
				<div className={css.button}>
					<DrawToggleButton click={props.drawerClickHandler}/>
				</div>
				<h2 id={navLogoId} className={top ? css.logo : css.logo_sticky}>
					<Link href="/">
						<a>
							<Image
								alt={"logo"}
								src={logo_color_small_png}
								width={100}
								height={100}
							/>
						</a>
					</Link>
				</h2>
				<div className={css.spacer}/>
				<nav className={`${css.nav_items} ${top ? css.nav_items_prim : css.nav_items_alter}`}>
					<MenuItems/>
				</nav>
			</div>
		</header>
	)
}
