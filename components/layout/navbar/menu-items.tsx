import Link from "next/link";
import {useI18n} from "next-localization";
import {getOtherLocale} from "../../../logic/utils/buildURL";
import {useRouter} from "next/router";
import css from "./menu-items.module.css";

export default function MenuItems() {
	const i18n = useI18n();

	const router = useRouter()

	return (
		<ul className={css.menu_list}>
			<li>
				<Link href="/">
					<a>
						{i18n.t('menu.main-page')}
					</a>
				</Link>
			</li>
			<li>
				<Link href="/blog">
					<a>
						{i18n.t('menu.blog')}
					</a>
				</Link>
			</li>
			<li>
				<a href="https://gameshelf.github.io?userId=igorz24&ownedgames=true" rel="noreferrer noopener" target="_blank">
					{i18n.t('menu.games')}
				</a>
			</li>
			<li>
				<Link href="/oferta">
					<a>
						{i18n.t('menu.offer')}
					</a></Link>
			</li>
			<li>
				<Link href="/o-nas">
					<a>
					{i18n.t('menu.about')}
				</a></Link>
			</li>
			<li>
				<Link href="/kontakt">
					<a>
					{i18n.t('menu.contact')}
				</a></Link>
			</li>
			<li>
				<Link href={router.pathname} locale={getOtherLocale(router.locale)}>
					<a className={css.flag_link}>
						{
							router.locale === 'pl' ?
								<img className={css.flag} alt="Change language to english" width={54} height={44}
								     src="https://catamphetamine.gitlab.io/country-flag-icons/3x2/GB.svg"/>
								: <img className={css.flag} alt="Zmień język na polski" width={54} height={44}
								       src="https://catamphetamine.gitlab.io/country-flag-icons/3x2/PL.svg"/>

						}
					</a>
				</Link>
			</li>
		</ul>
	);
}