import React from "react";
import styles from "./sticky-footer.module.css";
import Link from "next/link";
import {useI18n} from "next-localization";
import Obfuscate from 'react-obfuscate';


function Copyright() {
	return (
		<>
			<h1 className={styles.authors}>
				{"Design - Denis Mandrysz, Programming - Igor Zuber & Arkadiusz Oberaj"}
			</h1>
			<h1 className={styles.authors}>
				{"Copyright © "}
				<Link href="/">
					<a>
						{"Zalew Gier"}
					</a>
				</Link>{" 2019 - " + new Date().getFullYear() + "."}
			</h1>
		</>
	);
}

const StickyFooter = () => {

	const i18n = useI18n();

	return (
		<div className={styles.main_footer}>
			<div className={styles.container}>
				<div className={styles.col}>
					<h1>{i18n.t('footer.info')}</h1>
					<ul className={styles.list_unstyled}>
						<li>ul. Makowa 62</li>
						<li>44-213 Rybnik</li>
						<li>NIP 6423217632</li>
						<li>tel. 515 603 307</li>
						<li>
							<Obfuscate email="kontakt@zalewgier.pl"/>
						</li>
					</ul>
				</div>
				<div className={styles.col}>
					<h1>{i18n.t('footer.account')}</h1>
					<ul className={styles.list_unstyled}>
						<li>19 1140 2004 0000</li>
						<li>3102 7884 4417</li>
					</ul>
				</div>
			</div>
			<hr className={styles.break_line}/>
			<div className={styles.copyright}>
				<Copyright/>
			</div>
		</div>
	);
}

export default StickyFooter;
