import styles from "./layout.module.css";
import React from "react";

export default function Layout({children, home}: { children: React.ReactNode; home?: boolean }) {

	return (<>
			<div className={styles.container}>
				<main>{children}</main>
			</div>
		</>
	);
}
