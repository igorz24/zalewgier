import React from "react";
import css from "./headers.module.css";


export default function OneLineHeader({textLine}: { textLine: string }) {
	return (
		<>
			<h1 className={css.header_container_left}>
				<div className={css.max_fill_container}>
					<div className={css.left_line}/>
				</div>
				<p>
					{textLine}
				</p>
				<div className={css.max_fill_container}>
					<div className={css.right_line}/>
				</div>
			</h1>
		</>
	)
}