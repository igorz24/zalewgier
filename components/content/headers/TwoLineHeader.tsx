import React from "react";
import css from "./headers.module.css";


export default function TwoLineHeader({firstLine, secondLine}: { firstLine: string, secondLine: string }) {
	return (
		<>
			{/*FIRST LINE*/}
			<h1 className={css.header_container_left}>
				<div className={css.left_line_container}>
					<div className={css.left_line}/>
				</div>
				<p>
					{firstLine}
				</p>
			</h1>
			{/*SECOND LINE*/}
			<h1 className={css.header_container_right}>
				<p>
					{secondLine}
				</p>
				<div className={css.right_line_container}>
					<div className={css.right_line}/>
				</div>
			</h1>
		</>
	)
}