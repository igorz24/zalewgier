import styles from "./vertical-content-container.module.css";
import React from "react";

export default function VerticalContentContainer({children, webpSupport, displayLogo = true}: { children?: React.ReactNode, webpSupport: boolean, displayLogo?: boolean}) {
  return (
      <div className={`${styles.panel} ${!displayLogo?"":!webpSupport?styles.png:styles.webp}`}>
        {children}
      </div>
  );
}
