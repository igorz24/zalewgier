import css from "./blogindex-container.module.css";
import React, {useEffect} from "react";
import {useI18n} from "next-localization";
import Link from "next/link";
import {useRouter} from "next/router";
import {paperSheetsCoverPhotoSmall, paperSheetsCoverPhotoSmallWebp} from "../../../photo-lists/paper-sheets";
import {pangeaCoverPhotoSmall, pangeaCoverPhotoSmallWebp} from "../../../photo-lists/pangea-prototype";
import {ImgWithFallback} from "../images/ImageWithFallback";
import {olekCoverPhoto, olekCoverPhotoSmall, olekCoverPhotoSmallWebp} from "../../../photo-lists/jagodzinski-interview";
import {podsumowanie2020coverSmall, podsumowanie2020coverSmallWebp} from "../../../photo-lists/podsumowanie-2020";
import {bbCoverPhotoSmall, bbCoverPhotoSmallWebp} from "../../../photo-lists/blood-bowl";
import {podsumowanie2021coverSmall, podsumowanie2021coverSmallWebp} from "../../../photo-lists/podusmowanie-2021";
import {podsumowanie2022coverSmall, podsumowanie2022coverSmallWebp} from "../../../photo-lists/podsumowanie-2022";

interface IndexElement {
	url: string;
	localePath: string;
	image: any;
	image_webp: string
	key: string;
	langs: string[]
}

const index: IndexElement[] = [
	{
		key: "papierowe-notesy-1",
		url: "/blog/papierowe-notesy-1",
		localePath: "blog.papierowe-notesy-1.title",
		image: paperSheetsCoverPhotoSmall,
		image_webp: paperSheetsCoverPhotoSmallWebp,
		langs: ["pl", "en"]
	},
	{
		url: "/blog/pangea-recenzja-prototypu-2",
		localePath: "blog.pangea-recenzja-prototypu-2.title",
		image: pangeaCoverPhotoSmall,
		key: "pangea-recenzja-prototypu-2",
		image_webp: pangeaCoverPhotoSmallWebp,
		langs: ["pl", "en"]
	},
	{
		url: "/blog/jagodzinski-aleksander-3",
		localePath: "blog.jagodzinski-aleksander-3.title",
		image: olekCoverPhotoSmall,
		key: "jagodzinski-aleksander-3",
		image_webp: olekCoverPhotoSmallWebp,
		langs: ["pl", "en"]
	},
	{
		url: "/blog/podsumowanie-2020-4",
		localePath: "blog.podsumowanie-2020-4.title",
		image: podsumowanie2020coverSmall,
		key: "podsumowanie-2020-4",
		image_webp: podsumowanie2020coverSmallWebp,
		langs: ["pl"]
	},
	{
		url: "/blog/blood-bowl-5",
		localePath: "blog.blood-bowl-5.title",
		image: bbCoverPhotoSmall,
		key: "blood-bowl-5",
		image_webp: bbCoverPhotoSmallWebp,
		langs: ["pl"]
	},
  {
    url: "/blog/podsumowanie-2021-6",
    localePath: "blog.podsumowanie-2021-6.title",
    image: podsumowanie2021coverSmall,
    key: "podsumowanie-2021-6",
    image_webp: podsumowanie2021coverSmallWebp,
    langs: ["pl"]
  },
	{
		url: "/blog/podsumowanie-2022-7",
		localePath: "blog.podsumowanie-2022-7.title",
		image: podsumowanie2022coverSmall,
		key: "podsumowanie-2022-7",
		image_webp: podsumowanie2022coverSmallWebp,
		langs: ["pl"]
	}
]

export default function BlogIndexContainer({children}: { children?: React.ReactNode, lang?: any, langDict?: any }) {
	const i18n = useI18n();
	const router = useRouter()

	useEffect(() => {
		async function changeLocale() {
			if (router.locale === 'pl') {
				i18n.set('pl', await import('../../../locales/pl.json'));
				i18n.locale('pl');
			} else if (router.locale === 'en') {
				i18n.set('en', await import('../../../locales/en.json'));
				i18n.locale('en');
			}
		}
		changeLocale();
	}, [router.locale]);

	return (
		<div className={css.index}>
			{children}
			{index.slice(0).filter(item => item.langs.includes(router.locale)).reverse().map(item => (
					<div className={css.index_element} key={item.key}>
						<Link href={item.url}>
							<a>
								<div>
									<p>
										{i18n.t(item.localePath)}
									</p>
								</div>
								<ImgWithFallback src={item.image_webp} alt={i18n.t(item.localePath)} fallback={item.image} width={525} height={350}/>
							</a>
						</Link>
					</div>
				)
			)}
		</div>
	);
}