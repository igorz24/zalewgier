import React from "react";
import css from "./circle-photo.module.css"
import LazyLoad from 'react-lazyload';
import Image from "next/future/image";


export default function CirclePhoto({src, alt}: { src: string, alt: string }) {
	return (
		<LazyLoad offset={300} height={600} once>
			<div className={css.image_container}>
				<Image src={src} alt={alt} width={900} height={900} className={css.image_circle}/>
			</div>
		</LazyLoad>
	);
}
