import css from "./blogpost-container.module.css";
import React from "react";

export default function BlogPostContainer({children, webpSupport}: { children?: React.ReactNode, webpSupport: boolean }) {
	return (
		<div className={`${css.panel} ${!webpSupport ? css.png : css.webp}`}>
			{children}
		</div>
	);
}
