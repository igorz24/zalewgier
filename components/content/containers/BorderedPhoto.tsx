import React from "react";
import css from "./bordered-photo.module.css"
import LazyLoad from 'react-lazyload';
import Image from "next/future/image";


export default function BorderedPhoto({src, alt}: { src: any, alt: string, isSquare?: boolean }) {
	return (
		<LazyLoad offset={300} height={600} once>
			<div className={css.image_container}>
				<Image src={src} alt={alt} width={1800} height={1200} className={css.image}/>
			</div>
		</LazyLoad>
	);
}
