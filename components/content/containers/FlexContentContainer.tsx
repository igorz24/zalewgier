import styles from "./flex-content-container.module.css";
import React from "react";

export default function FlexContentContainer({children}: { children?: React.ReactNode}) {
	return (
		<div className={styles.flex_container}>
			{children}
		</div>
	);
}
