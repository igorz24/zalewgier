import React from "react";
import css from "./paragraph-with-dice.module.css"
import {dice_orange_png} from "../../../logic/utils/logos";
import Image from 'next/image'

export default function ParagraphWithDice({children}: { children?: React.ReactNode }) {
  return (
      <div className={css.diced_line}>
        <Image src={dice_orange_png} width={40} height={40} alt="dice_orange" className={css.dice}/>
        <p>{children}</p>
        <Image src={dice_orange_png} width={40} height={40} alt="dice_orange" className={css.dice}/>
      </div>
  );
}



