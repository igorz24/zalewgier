import {ParallaxBanner} from "react-scroll-parallax";
import css from "./parallax.module.css";
import React from "react";

export default function Parallax({children, image, styles, topGradient = false, bottomGradient = false, amount}:
																	 { children?: React.ReactNode, image: any, styles?: any, topGradient?: boolean, bottomGradient?: boolean, amount: number}) {

	let gradientClasses = `${css.gradient_base} `;
	if (topGradient && !bottomGradient)
		gradientClasses += `${css.gradient_top} `;
	else if (bottomGradient && !topGradient)
		gradientClasses += `${css.gradient_bottom}`;
	else if (bottomGradient && topGradient)
		gradientClasses += `${css.gradient_top_bottom}`;
	return (
		<ParallaxBanner className={css.parallax_banner}
			layers={[
				{
					image: image,
          speed: amount
				}
			]}
			style={{
				height: '70vh',
				...styles
			}}
		>
			<div className={gradientClasses}>
				{children}
			</div>
		</ParallaxBanner>
	)
}