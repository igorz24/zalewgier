import css from "./facebook-button.module.css";
import React from "react";
import Link from "next/link";
import useHover from "@react-hook/hover";
import {number} from "prop-types";

interface PhoneButtonProps {
	number: string;
	styles?: any;
}

export default function PhoneButton(props: PhoneButtonProps) {

	return (
		<div className={css.button_container} style={props.styles}>
			<Link href="//www.facebook.com/zalewgier">
				<a href={`tel:+48${props.number}`} id="fb-button" className={css.facebook_button}>
					{props.number}
				</a>
			</Link>
		</div>)
}