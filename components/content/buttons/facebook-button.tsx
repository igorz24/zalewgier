import css from "./facebook-button.module.css";
import React from "react";
import Link from "next/link";
import useHover from "@react-hook/hover";

interface FacebookButtonProps {
	styles?: any;
}

export default function FacebookButton(props: FacebookButtonProps) {
	const target = React.useRef(null)
	const isHovering = useHover(target)

	return (
		<div className={css.button_container} style={props.styles} >
			<Link href="https://www.facebook.com/zalewgier">
				<a ref={target} id="fb-button" className={css.facebook_button}>
					{isHovering ? 'Zalew Gier' : 'Facebook'}
				</a>
			</Link>
		</div>)
}