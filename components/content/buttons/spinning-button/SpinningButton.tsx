import css from "./spinning-button.module.css";

export default function SpinningButton() {
	return (
		<div className={`${css.section} ${css.button_container}`}>
			<div className={css.fb_button}>
				<p className={css.text}>facebook</p>
				<div className={`${css.ring} ${css.one}`}/>
				<div className={`${css.ring} ${css.two}`}/>
				<div className={`${css.ring} ${css.three}`}/>
				<div className={`${css.ring} ${css.four}`}/>
			</div>
		</div>)
}