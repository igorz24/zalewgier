import React from "react";

export const ImgWithFallback = ({src, fallback, type = 'image/webp', alt, className, width, height}: {
	src: any, alt: string, fallback: string, type?: string, className?: string, width?: number | string, height?: number | string
}) => {
	return (
		<picture>
			<source srcSet={src} type={type}/>
			<img className={className} src={fallback} alt={alt} width={width} height={height}/>
		</picture>
	);
};