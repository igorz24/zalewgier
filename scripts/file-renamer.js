// Script renames all files from "X.jpg" to "Xs.jpg"

// Import filesystem module
const fs = require('fs');

// List all the filenames before renaming
getCurrentFilenames("public/images/7-podsumowanie2022/s");

// Function to get current filenames
// in directory
function getCurrentFilenames(dirname) {
    console.log("Current filenames:");
    fs.readdirSync(__dirname + "/" + dirname).forEach(file => {
        fs.renameSync(
            __dirname + "/" + dirname + "/" + file,
            __dirname + "/" + dirname + "/" +file.substring(0, file.length - 4).concat("s.jpg"))
    });
}