const sitemap = require('nextjs-sitemap-generator');
const fs = require("fs");

const BUILD_ID = fs.readFileSync(".next/BUILD_ID").toString();

sitemap({
    baseUrl: 'https://zalewgier.pl',
    alternatesUrls: {
        en: 'https://zalewgier.pl/en'
    },
    pagesDirectory: __dirname + "/.next/server/pages",
    targetDirectory: 'public/',
    ignoredPaths: ["[fallback]"],
});

console.log(`✅ sitemap.xml generated!`);
console.log(`remember to manually replace /pl in paths and delete 404s!`);

