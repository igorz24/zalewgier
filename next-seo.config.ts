import {NextSeoProps} from "next-seo";

const defaultSeo: NextSeoProps = {
	titleTemplate: "%s | Zalew Gier",
	twitter: {
		cardType: 'summary_large_image',
		handle: '@handle',
		site: '@site',
	},

};

export default defaultSeo;