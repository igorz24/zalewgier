const withPlugins = require('next-compose-plugins');
const withFonts = require('next-fonts');

const nextJsConfig = {
  i18n: {
    // These are all the locales you want to support in
    // your application
    locales: ['pl', 'en'],
    // This is the default locale you want to be used when visiting
    // a non-locale prefixed path e.g. `/hello`
    defaultLocale: 'pl',
    localeDetection: true
  },
  experimental: {
    images: {
      allowFutureImage: true
    }
  }
}

module.exports = withPlugins([
  withFonts
], nextJsConfig)


