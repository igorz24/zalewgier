const image13 = "/images/3-jagodzinski/13.jpg"
const s_image13 = "/images/3-jagodzinski/13s.jpg"
const image13_webp = "/images/3-jagodzinski/13.webp"
const s_image13_webp = "/images/3-jagodzinski/13s.webp"

export const olekPhotoAlts: string[] = [
	"Aleksander Jagodziński",
	"Żercy illustration",
	"Żercy illustration",
	"Żercy illustration",
	"Żercy illustration",
	"Żercy illustration",
	"Pangea game box",
	"Pangea card",
	"Pangea card",
	"Pangea cards",
	"Pangea cards",
	"Pangea game box",
	"KUR-NU-GI illustration",
	"KUR-NU-GI game box",
	"KUR-NU-GI illustration"
];

export {
	image13 as olekCoverPhoto,
	s_image13 as olekCoverPhotoSmall,
	image13_webp as olekCoverPhotoWebp,
	s_image13_webp as olekCoverPhotoSmallWebp,
}