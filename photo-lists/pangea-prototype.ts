const image20 = "/images/2-pangea/20.jpg"
const s_image20 = "/images/2-pangea/20s.jpg"
const image20_webp = "/images/2-pangea/20.webp"
const s_image20_webp = "/images/2-pangea/20s.webp"

export const pangeaPhotoAlts: string[] = [
	"Pangea"
	, "Pangea playtesting"
	, "Pangea board"
	, "Pangea playtesting"
	, "Cataclysm card"
	, "Ecological niche"
	, "Game board"
	, "Rounds track"
	, "Species board"
	, "Event cards"
	, "Single event card"
	, "Evolution cards"
	, "Evolution cards"
	, "Evolution cards"
	, "Evolution cards"
	, "Evolution cards"
	, "Game components"
	, "Game components"
	, "Game components"
	, "Game components"
	, "Game components"
	, "Game components"
	, "Game components"
	, "Game components"
	, "Species boards"
	, "Synapsides"
	, "Zauropsides"
	, "Amphibians"
	, "Invertebrates"
	, "Playtesting"
	, "Game board"
	, "Game components"
	, "Game components"
	, "Game components"
	, "Unreleased game components"
	, "Unreleased game components"
	, "Two player game"
];

export {
	image20 as pangeaCoverPhoto,
	s_image20 as pangeaCoverPhotoSmall,
	image20_webp as pangeaCoverPhotoWebp,
	s_image20_webp as pangeaCoverPhotoSmallWebp,
}