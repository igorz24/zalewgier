const cover = "/images/7-podsumowanie2022/5.jpg"
const s_cover = "/images/7-podsumowanie2022/5.jpg"
const cover_webp = "/images/7-podsumowanie2022/5.webp"
const s_cover_webp = "/images/7-podsumowanie2022/5.webp"

export const podsumowanie2022PhotoAlts: string[] = [
    "Terraformacja Marsa: Niepokoje",
    "Gracze przy grze Ark Nova",
    "Rozgrywka w Minerały",
    "Root",
    "Klany Kaledonii",
    "Uśmiechnięci gracze z pluszakami w kształcie meepli",
    "Krzesełka z Mistakos",
    "Turniej Munchkina",
    "Turniej Neuroshimy Hex",
    "Turniej Neuroshimy Hex",
    "Zwycięzca mini turnieju Draftozaura",
    "Turniej Catana",
    "Zwycięzca mini turnieju Ekosystem",
    "Turniej Mistakos",
    "Noc Naukowców",
    "Nocny Maraton Gier Planszowych w 2 LO",
    "Grydcon",
    "Nocny Maraton Gier Planszowych w 2 LO - gra w Avalon",
    "3 Raciborski Festiwal Gamingowy",
    "Cyklady",
    "MikroMakro, Kaskadia, Hansa Teutonica",
    "Horrified",
    "Park Smoków",
    "Rising Sun",
    "Zakupy klubowe - Architekci, Nieustraszeni, Imperium: Legendy, Sidereal Confluence",
    "Informacja o Dotacji",
    "Informacja o 1% podatku",
];

export {
    cover as podsumowanie2022cover,
    s_cover as podsumowanie2022coverSmall,
    cover_webp as podsumowanie2022coverWebp,
    s_cover_webp as podsumowanie2022coverSmallWebp
}