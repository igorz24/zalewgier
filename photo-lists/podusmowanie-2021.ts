const cover = "/images/6-podsumowanie2021/23.jpg"
const s_cover = "/images/6-podsumowanie2021/23.jpg"
const cover_webp = "/images/6-podsumowanie2021/23.webp"
const s_cover_webp = "/images/6-podsumowanie2021/23.webp"

export const podsumowanie2021PhotoAlts: string[] = [
  "Regulamin spotkań",
  "Patchwork",
  "Ethnos",
  "Listy z Whitechapel",
  "Blood Bowl",
  "Gloomhaven",
  "Calico",
  "Nidavellir",
  "Welcome to...",
  "Nowości klubowe",
  "Nowości klubowe 2",
  "Nowości klubowe 3",
  "Nowości klubowe 4",
  "Wyprawa do El Dorado",
  "Sesja RPG",
  "Pub Spółdzielczy",
  "Planszówki w Spodku",
  "Planszówki w Spodku 2",
  "Planszówki w Spodku 3",
  "Lineon",
  "KUR-NU-GI",
  "Horror w Arkham",
  "Jaskinia Trolla",
  "1% podatku dla Zalewu Gier",
];

export {
  cover as podsumowanie2021cover,
  s_cover as podsumowanie2021coverSmall,
  cover_webp as podsumowanie2021coverWebp,
  s_cover_webp as podsumowanie2021coverSmallWebp
}