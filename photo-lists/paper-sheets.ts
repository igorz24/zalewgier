const image18 = "/images/1-notesy/18.jpg"
const image19 = "/images/1-notesy/19.jpg"
const s_image18 = "/images/1-notesy/18s.jpg"
const s_image19 = "/images/1-notesy/19s.jpg"
const image18_webp = "/images/1-notesy/18.webp"
const image19_webp = "/images/1-notesy/19.webp"
const s_image18_webp = "/images/1-notesy/18s.webp"
const s_image19_webp = "/images/1-notesy/19s.webp"
import {PhotoWebP} from "../components/gallery/ImagesGallery";

export const paperSheetsPhotoAlts: string[] = [
	"Raw paper sheets"
	, "Welcome too..."
	, "Nie do pary"
	, "Bukiet"
	, "Steam Rollers"
	, "Downforce - Bolidy"
	, "Dry erase markers"
	, "Dry erase markers"
	, "Dry erase markers"
	, "Cutting out laminated sheets"
	, "Raw sheets"
	, "Badly laminated sheets"
	, "Laminating process"
	, "Laminating process"
	, "Laminating process"
	, "Cutting out Steam Rollers sheets"
	, "Miscuts"
	, "Welcome too..."
	, "Laminated sheets"
];

export const paperSheetPhotos5: PhotoWebP[] = [
	{
		src: image18,
		srcSet: s_image18,
		src_webp: image18_webp,
		srcSet_webp: s_image18_webp,
		width: 3,
		height: 2,
		alt: "Laminated boards for Welcome too..."
	},
	{
		src: image19,
		srcSet: s_image19,
		src_webp: image19_webp,
		srcSet_webp: s_image19_webp,
		width: 3,
		height: 2,
		alt: "Laminated sheets of various games"
	}
];

export {
	image18 as paperSheetsCoverPhoto,
	s_image18 as paperSheetsCoverPhotoSmall,
	image18_webp as paperSheetsCoverPhotoWebp,
	s_image18_webp as paperSheetsCoverPhotoSmallWebp
}