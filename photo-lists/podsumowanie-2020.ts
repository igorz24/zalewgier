const cover = "/images/4-podsumowanie2020/33.jpg"
const s_cover = "/images/4-podsumowanie2020/33s.jpg"
const cover_webp = "/images/4-podsumowanie2020/33.webp"
const s_cover_webp = "/images/4-podsumowanie2020/33s.webp"

export const podsumowaniePhotoAlts: string[] = [
  "Ishtar",
  "7 Cudów Świata",
  "Drako na planszy Carroma",
  "Prototyp KUR-NU-GI",
  "Konkurs Story Cubes",
  "Plakat mysiej straży",
  "Dungeon & Dragons",
  "Sesja RPG",
  "Vintage Cube",
  "Blood Bowl, zdjęcie z prywatnej kolekcji Dominika Dalbiaka",
  "Zalew Gier na Snowfall Ponymeet 2020",
  "Star Realms",
  "Turniej Hero Realms",
  "Gamesroom od Portal Games",
  "Gry od Tactic Games",
  "Zalew Gier w Ornontowicach",
  "Zalew Gier na Portalconie",
  "Zalew Gier na Portalconie 2",
  "Terrors of London od Portal Games",
  "Gry od Naszej Księgarni",
  "Gry od Portalu",
  "Pakiet Noworoczny od Rebela",
  "Tapestry",
  "Nowości klubowe",
  "Nowości klubowe 2",
  "Tiny Towns i Kartografowie",
  "Gry od Muduko",
  "Root Kickstarter",
  "Zalew Gier 1% podatku",
  "Ubrania do oddania",
  "Wsparcie fireup.pro",
  "fireup.pro i Zalew Gier",
  "Pionki z Roota"
];

export {
  cover as podsumowanie2020cover,
  s_cover as podsumowanie2020coverSmall,
  cover_webp as podsumowanie2020coverWebp,
  s_cover_webp as podsumowanie2020coverSmallWebp
}