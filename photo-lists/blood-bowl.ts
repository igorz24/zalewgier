const image1 = "/images/5-bloodbowl/1.jpg"
const s_image1 = "/images/5-bloodbowl/1s.jpg"
const image1_webp = "/images/5-bloodbowl/1.webp"
const s_image1_webp = "/images/5-bloodbowl/1s.webp"

export const bbPhotoAlts: string[] = [
	"Orkowie vs Gobliny na Mistrzostwach Polski 2017",
	"Drużyna Jaszczuroludzi Domingo",
	"Jedna z dwóch sal na mistrzostwach świata w Dornbirn 2019",
	"Chłopaki z klubu Pomorskie Trolle na mistrzostwach świata w Dornbirn 2019",
	"Drużynowe Mistrzostwa Polski w Łodzi 2019",
	"Mistrzostwa Europy w Porto 2017 i mecz Francja : Anglia",
	"Domingo i Shawass (klub Gamonie) na mistrzostwach świata w Dornbirn 2019",
	"Polska scena Blood Bowla",
	"Drużyna Shawassa",
];

export {
	image1 as bbCoverPhoto,
	s_image1 as bbCoverPhotoSmall,
	image1_webp as bbCoverPhotoWebp,
	s_image1_webp as bbCoverPhotoSmallWebp,
}