Static website project created with next.js, react and typescript, to see it live visit [Zalew Gier](https://zalewgier.pl/).

## Adding new posts:
- add new file under /pages/blog directory
- fill - localeKey, seo details
- locale - add new key and fill it with translations
- fill the post with content
- create dir for photos
- add photos to public/images/X, rename them 1-N, duplicate, rename 1s-Ns, batch resize for performance reasons -> https://bulkresizephotos.com/en
- prepare photo-lists file with alt descriptions and coverage image
- add photos to post
- seo - cover photo
- getStaticProps - dir path, pics count, alts
- update blog index
- update sitemap
- add basic translation to the other language to make everything compile