import {NextRouter} from "next/router";

export const currentLocalizedUrl = (router: NextRouter) => {
	return "https://zalewgier.pl" + (router.locale === "pl" ? "" : "/en") + router.pathname
}

export const getOtherLocale = (currentLocale: string): string => {
	return currentLocale === "pl" ? "en" : "pl";
}
