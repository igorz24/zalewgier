import React from "react";

export const prepareLinks = (text: string): any => {
	const regex = new RegExp("\\{|\\}")
	const parts = text.split(regex)
	return <>{
		parts.map(item => {
			if (item.includes("|")) {
				let parts = item.split("|")
				return <a target={"_blank"} href={parts[1]}>{parts[0]}</a>
			}
			else
				return item
		})
	}</>

}