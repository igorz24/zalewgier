import {Photo, PhotoWebP} from "../../components/gallery/ImagesGallery";

export const getGalleryPhotos = (photos: PhotoWebP[], start: number, end: number, webP = false): Photo[] => {
	if (webP)
		return photos.slice(start, end + 1).map(item => {
			return {
				...item,
				src: item.src_webp,
				srcSet: item.srcSet_webp
			}
		})

	else return photos.slice(start, end + 1)
}