export const COLORS = {
    BLUE_L: "#0a3851",
    BLUE_D: "#192227",
    ORANGE: "#F08319",
    WHITE: "#ffffff",
    BLACK: "#000000",
}