const logo_color_circle_png = "/images/logos/logo_color_circle.png";
const logo_color_png = "/images/logos/logo_color.png";
const logo_color_small_png = "/images/logos/logo_color_circle_small.png";
const logo_black_png = "/images/logos/logo_black.png";
const logo_white_png = "/images/logos/logo_white.png";
const logo_orange_png = "/images/logos/logo_orange.png";
const logo_blue_png = "/images/logos/logo_blue.png";
const dice_orange_png = "/images/logos/dice_orange.png";

export {
  logo_color_small_png,
  logo_black_png,
  logo_blue_png,
  logo_color_circle_png,
  logo_color_png,
  logo_orange_png,
  logo_white_png,
  dice_orange_png,
};
