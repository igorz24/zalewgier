import {PhotoWebP} from "../../components/gallery/ImagesGallery";

export const gallery = (count: number, directory: string, alts: string[]): PhotoWebP[] => {
	const photos: PhotoWebP[] = [];
	for (let i = 1; i <= count; i++) {
		photos.push(
			{
				src: `/images/${directory}/${i}.jpg`,
				srcSet: `/images/${directory}/${i}s.jpg`,
				src_webp: `/images/${directory}/${i}.webp`,
				srcSet_webp: `/images/${directory}/${i}s.webp`,
				width: undefined,
				height: undefined,
				alt: alts[i-1]
			}
		);
	}
	return photos;
}