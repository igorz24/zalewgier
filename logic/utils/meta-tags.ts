import dict_pl from "../../locales/pl.json"
import dict_en from "../../locales/en.json"

const resolve = (path: string, obj: any, separator='.') => {
	const properties = Array.isArray(path) ? path : path.split(separator)
	return properties.reduce((prev, curr) => prev && prev[curr], obj)
}

// Because next-localization package fetches translations AFTER the page sources are rendered,
// webscrapers don't see meta tags and we need to provide them this way
export const getMeta = (locale: string, localePath: string): {title: string, desc: string} => {
	return locale === "pl" ?
		resolve(localePath, dict_pl) :
		resolve(localePath, dict_en)
}